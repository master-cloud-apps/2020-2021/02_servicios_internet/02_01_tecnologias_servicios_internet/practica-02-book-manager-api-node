const mongoose = require('mongoose')

const connect = async(uri) => {
  const mongooseOpts = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  }

  const connection = await mongoose.connect(uri, mongooseOpts)
  return connection
}

module.exports = { connect }
