const express = require('express')
const { books } = require('./../endpoints/index.js')

const router = new express.Router()
const bookHandler = books()

router.post('/books', bookHandler.post)
router.get('/books', bookHandler.get)
router.get('/books/:bookId', bookHandler.getById)
router.delete('/books/:bookId', bookHandler.deleteById)
router.put('/books/:bookId', bookHandler.updateById)

module.exports = router
