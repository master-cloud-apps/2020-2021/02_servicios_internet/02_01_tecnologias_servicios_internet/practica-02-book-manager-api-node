const request = require('supertest')

const bookTest = {
  title: 'Test title',
  yearPublication: 1974,
  editorial: 'Test editorial',
  review: 'This is a test book, what do you want?',
  author: 'Alberto Eyo'
}
const createBook = ({ book, app }) => request(app).post('/books').send(book)
const deleteBookById = ({ bookId, app }) => request(app).delete(`/books/${bookId}`)
const getBookById = ({ bookId, app }) => request(app).get(`/books/${bookId}`)
const createTestBook = (app) => createBook({ book: bookTest, app })

module.exports = {
  bookTest, createBook, createTestBook, deleteBookById, getBookById
}
