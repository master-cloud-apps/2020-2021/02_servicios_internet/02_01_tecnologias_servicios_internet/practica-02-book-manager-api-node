const request = require('supertest')
const app = require('./../../../src/app/index.js')
const { getIdFromResponseLocationHeader, manageInMemoryDatabase } = require('./../index.js')
const { expect } = require('chai')
const { createTestBook, bookTest, deleteBookById } = require('../book.js')
const _ = require('lodash')

describe('Book use cases', () => {
  manageInMemoryDatabase()

  describe('PUT /books/:bookId use cases', () => {
    let bookTestId
    beforeEach(() => createTestBook(app)
      .then(response => getIdFromResponseLocationHeader(response))
      .then(bookId => (bookTestId = bookId)))
    it('When update by id wrong id should return 400', () => {
      return request(app).put('/books/12345').send({})
        .then(response => expect(response.statusCode).to.equal(400))
    })
    it('Given book created, when update by id title should return 200', () => {
      const updatedBook = _.cloneDeep(bookTest)
      updatedBook.title = 'Updated Title'
      return request(app).put(`/books/${bookTestId}`).send(updatedBook)
        .then(response => {
          expect(response.statusCode).to.equal(200)
          expect(response.body.title).to.be.equal('Updated Title')
        })
    })
    it('Given book created, when update by id not allowed field should return 400', () => {
      const updatedBook = _.cloneDeep(bookTest)
      updatedBook.invalidFieldToUpdate = 'Updated Title'
      return request(app).put(`/books/${bookTestId}`).send(updatedBook)
        .then(response => {
          expect(response.statusCode).to.equal(400)
          expect(response.body.error).to.be.equal('Invalid update field')
        })
    })
    it('Given book deleted, when update by id should return not found', () => {
      const updatedBook = _.cloneDeep(bookTest)
      updatedBook.invalidFieldToUpdate = 'Updated Title'
      return deleteBookById({ bookId: bookTestId, app })
        .then(response => expect(response.statusCode).to.be.equal(204))
        .then(_ => request(app).put(`/books/${bookTestId}`).send({}))
        .then(response => expect(response.statusCode).to.equal(404))
    })
  })
})
