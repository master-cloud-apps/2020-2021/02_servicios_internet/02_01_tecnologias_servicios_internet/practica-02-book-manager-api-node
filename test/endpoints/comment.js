const request = require('supertest')
const { createModel } = require('./index.js')

const getCommentTest = nick => ({
  content: 'Comment content for testing',
  punctuation: 3.5,
  nick
})
const createComment = ({ comment, bookId, app }) => createModel({ model: comment, modelType: 'comment', bookId, app })
const deleteCommentById = ({ bookId, commentId, app }) => request(app).delete(`/books/${bookId}/comments/${commentId}`)
const getCommentById = ({ bookId, commentId, app }) => request(app).get(`/books/${bookId}/comments/${commentId}`)
const createTestComment = ({ app, nick, bookId }) => createComment({ comment: getCommentTest(nick), app, bookId })

module.exports = {
  createComment, deleteCommentById, getCommentById, createTestComment, getCommentTest
}
