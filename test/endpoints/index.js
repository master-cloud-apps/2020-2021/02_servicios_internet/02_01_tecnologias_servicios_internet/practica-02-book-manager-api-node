const dbHandler = require('./../db-handler.js')
const request = require('supertest')

const getIdFromLocation = location =>
  location.split('/')[location.split('/').length - 1]

const getIdFromResponseLocationHeader = response =>
  getIdFromLocation(response.headers.location)

const manageInMemoryDatabase = () => {
  before(() => dbHandler.connect())

  afterEach(() => dbHandler.clearDatabase())

  after(() => dbHandler.closeDatabase())
}

const getUrlByModel = ({ modelType, bookId = undefined }) => {
  const urlByModel = {
    book: '/books',
    comment: `/books/${bookId}/comments`,
    user: '/users'
  }
  return urlByModel[modelType]
}

const createModel = ({ model, modelType, bookId = undefined, app }) =>
  request(app).post(getUrlByModel({ modelType, bookId })).send(model)

module.exports = {
  getIdFromResponseLocationHeader, manageInMemoryDatabase, createModel
}
