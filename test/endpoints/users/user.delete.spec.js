const request = require('supertest')
const app = require('./../../../src/app/index.js')
const { manageInMemoryDatabase, getIdFromResponseLocationHeader } = require('./../index.js')
const { createTestUserAndGetId, getUserById } = require('./../user.js')
const { createTestComment, getCommentById } = require('./../comment.js')
const { createTestBook } = require('./../book.js')
const { expect } = require('chai')

describe('User use cases', () => {
  manageInMemoryDatabase()

  describe('DELETE /users/:userId use cases', () => {
    it('Given user created, when delete by id should return 204', () => {
      return createTestUserAndGetId(app)
        .then(userId => request(app).delete(`/users/${userId}`))
        .then(response => expect(response.statusCode).to.equal(204))
    })
    it('Given user created, when delete by id wrong id should return bad request', () => {
      return request(app).delete('/users/12345')
        .then(response => expect(response.statusCode).to.equal(400))
    })
    it('Given created user when delete user then get user by id should return 404', () => {
      let userTestId
      return createTestUserAndGetId(app)
        .then(userId => (userTestId = userId))
        .then(() => request(app).get(`/users/${userTestId}`))
        .then(response => expect(response.statusCode).to.be.equal(200))
        .then(() => request(app).delete(`/users/${userTestId}`))
        .then(response => expect(response.statusCode).to.be.equal(204))
        .then(() => request(app).get(`/users/${userTestId}`))
        .then(response => expect(response.statusCode).to.be.equal(404))
    })
    it('Given created user when delete user twice then second time should return 404', () => {
      let userTestId
      return createTestUserAndGetId(app)
        .then(userId => (userTestId = userId))
        .then(() => request(app).get(`/users/${userTestId}`))
        .then(response => expect(response.statusCode).to.be.equal(200))
        .then(() => request(app).delete(`/users/${userTestId}`))
        .then(response => expect(response.statusCode).to.be.equal(204))
        .then(() => request(app).delete(`/users/${userTestId}`))
        .then(response => expect(response.statusCode).to.be.equal(404))
    })
    describe('Given user, book and comment created', () => {
      let testBookId
      let user
      let testComment
      beforeEach(() => createTestBook(app)
        .then(response => getIdFromResponseLocationHeader(response))
        .then(bookId => (testBookId = bookId))
        .then(() => createTestUserAndGetId(app))
        .then(userId => getUserById({ userId, app }))
        .then(response => (user = response.body))
        .then(() => createTestComment({ app, nick: user.nick, bookId: testBookId }))
        .then(response => getIdFromResponseLocationHeader(response))
        .then(commentId => getCommentById({ bookId: testBookId, commentId, app }))
        .then(createdComment => (testComment = createdComment.body)))
      it('When delete should return bad request', () => {
        return request(app).delete(`/users/${user.id}`)
          .then(response => {
            expect(response.statusCode).to.be.equal(400)
          })
      })
    })
  })
})
