const request = require('supertest')
const app = require('./../../../src/app/index.js')
const { manageInMemoryDatabase } = require('./../index.js')
const { testUser, createTestUserAndGetId } = require('./../user.js')
const { expect } = require('chai')
const _ = require('lodash')

describe('User use cases', () => {
  manageInMemoryDatabase()

  describe('PUT /users/:userId use cases', () => {
    it('Given user created, when update user email should return ok', async() => {
      const userToUpdate = _.cloneDeep(testUser)
      userToUpdate.email = 'updated_email@test.com'
      delete userToUpdate.nick
      const userId = await createTestUserAndGetId(app)

      const responsePut = await request(app).put(`/users/${userId}`).send(userToUpdate)

      expect(responsePut.statusCode).to.be.equal(200)
    })
    it('Given user created, when update user email should return user with email modified', async() => {
      const userToUpdate = _.cloneDeep(testUser)
      userToUpdate.email = 'updated_email@test.com'
      delete userToUpdate.nick
      const userId = await createTestUserAndGetId(app)

      const responsePut = await request(app).put(`/users/${userId}`).send(userToUpdate)

      expect(responsePut.body.email).to.be.equal('updated_email@test.com')
    })
    it('Given user updated when get user by id should return user with email modified', async() => {
      const userToUpdate = _.cloneDeep(testUser)
      userToUpdate.email = 'updated_email@test.com'
      delete userToUpdate.nick
      const userId = await createTestUserAndGetId(app)
      await request(app).put(`/users/${userId}`).send(userToUpdate)

      const responseGet = await request(app).get(`/users/${userId}`)

      expect(responseGet.body.email).to.be.equal('updated_email@test.com')
    })
    it('Given user created, when update user nick should return bad request', async() => {
      const userToUpdate = _.cloneDeep(testUser)
      userToUpdate.nick = 'updated_nick'
      const userId = await createTestUserAndGetId(app)

      const response = await request(app).put(`/users/${userId}`).send(userToUpdate)

      expect(response.statusCode).to.be.equal(400)
    })
    it('when update user with not found id should return not found', async() => {
      const userToUpdate = _.cloneDeep(testUser)
      userToUpdate.nick = 'updated_nick'
      const userId = await createTestUserAndGetId(app)

      const response = await request(app).put(`/users/${userId}`).send(userToUpdate)

      expect(response.statusCode).to.be.equal(400)
    })
    it('Given user deleted, when update by id should return not found', () => {
      // const updatedBook = _.cloneDeep(testUser)
      // updatedBook.invalidFieldToUpdate = 'Updated Title'
      // return deleteBookById({ bookId: bookTestId, app })
      //   .then(response => expect(response.statusCode).to.be.equal(204))
      //   .then(_ => request(app).put(`/books/${bookTestId}`).send({}))
      //   .then(response => expect(response.statusCode).to.equal(404))
    })
  })
})
